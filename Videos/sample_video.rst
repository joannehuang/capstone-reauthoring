=====================================
 Embedding Multiple Videos in a Page
=====================================

The videos are simply included with the directive previously described. You may
want to write some additional information around the videos such as supporting
information, or the link directly to YouTube.

China Rock & Roll: Live in Hong Kong 1994
-------------------------------------

The `following video is China Rock & Roll: Live in Hong Kong 1994
<https://www.youtube.com/watch?v=Q_b69qqAP0c>`_.

.. embedded-video:: Q_b69qqAP0c

-  :download:`The annotations produced during the video 
   <COD_baseencoding_videoeqt.pdf>`

Dou Wei: Be good, boy.
------------------------------------------

The `following video is China Rock & Roll: Live in Hong Kong 1994
, and from 7:42 is the song of Dou Wei "Be good, boy" <https://www.youtube.com/watch?v=Q_b69qqAP0c>`_.

.. embedded-video:: Q_b69qqAP0c 462

- :download:`The annotations produced during the video
  <COD_integerencoding_videoeqt.pdf>`

