from django.http import HttpResponse
import json
def json_response(func):
    """
    A decorator thats takes a view response and turns it
    into json. If a callback is added through GET or POST
    the response is JSONP.
    """
    # def decorator(request, *args, **kwargs):
    def decorator(request, *callback_args, **callback_kwargs):
        objects = func(request, *callback_args, **callback_kwargs)
        a = str(objects.result_count)
        data = "isAgoodgirl({'result_count': " + a + ",'total_attempt':2 });"
        return HttpResponse(data, "application/text")
        # if isinstance(objects, HttpResponse):
        #     return objects
       # try:
        # data = json.dumps(objects)
            # if 'result_count' in request.REQUEST:
                # a jsonp response!
       # data = 'isAgoodgirl( {"result_count":1,"total_attempt":2 });'


        # except:
        #     data = json.dumps(str(objects))
       # return HttpResponse(data,"application/json")
    return decorator