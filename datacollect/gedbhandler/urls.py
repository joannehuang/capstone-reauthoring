from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^answer/receive/', views.AnswerReceiveView.as_view(), name='answer_receive'),
    url(r'^result/count/', views.ResultCountView.as_view(), name='result_count'),
    url(r'^result/url/', views.ResultUrlView.as_view(), name='result_url'),
]