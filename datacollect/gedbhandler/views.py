# -*- coding: utf-8 -*-
# from django.template import loader
# import json_response
# import json
# from django.http import JsonResponse
# from .decorators import json_response

from __future__ import unicode_literals
from __future__ import division
from django.shortcuts import render
from django.views import View
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views import generic
from django.http import HttpResponse
from .models import Answer, Student, Teacher, Result, Course


# The view for display the list of data
class IndexView(generic.ListView):
    template_name = 'gedbhandler/index.html'
    model = Answer

    def get_context_data(self,**kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        latest_answer_list = Answer.objects.order_by('id')[:]
        total = 0
        for a in latest_answer_list:
            total = total + a.answer
        context['latest_answer_list'] = latest_answer_list
        context['total']= total
        return context

# the view to manipulate the received information and database
class AnswerReceiveView(View):
    def get(self,request,*args, **kwargs):
        question_id = str(request.GET.get('question_id'))
        answer = request.GET.get('answer')
        if answer == None :
            return render(request, 'gedbhandler/answer_receive.html')
        q = Answer(question_id = question_id, answer = answer,course_id = 1 )
        q.save()
        return render(request, 'gedbhandler/answer_receive.html')

# The view to transfer back the result
class ResultUrlView(View):
    def get(self,request,*args, **kwargs):
        latest_answer_list = Answer.objects.order_by('id')[:]
        total = 0
        total_attempt = 0
        for a in latest_answer_list:
            total = total + a.answer
            total_attempt = total_attempt + 1
        if request.method == 'GET' :
            data_total = str(total)
            data_attempt = str(total_attempt)
            result = str('isAgoodgirl( {"result_count":'+ data_total
                         + ',"total_attempt":'+ data_attempt + '});')
            return HttpResponse(result)

# @csrf_exempt
class ResultCountView(View):
    def get(self,request,*args, **kwargs):
        mul_one_total = 0
        mul_one_correct= 0
        mul_mul_total = 0
        mul_mul_correct = 0
        short_ans_total = 0
        short_ans_correct = 0
        latest_answer_list = Answer.objects.order_by('id')[:]
        # mul_one_list = Answer.objects.get(question_id="COD-integerencoding-videoeqt-eqt_11")
        # mul_mul_list = Answer.objects.get(question_id="COD - hexanane - 11")
        # short_ans_list = Answer.objects.get(question_id="COD-integerencoding-sequence-11")

        for a in latest_answer_list:
            if a.question_id == "COD-integerencoding-videoeqt-eqt_11":
                mul_one_total = mul_one_total + 1
                mul_one_correct = mul_one_correct + a.answer
            elif a.question_id == "COD-hexanane-11":
                mul_mul_total = mul_mul_total +1
                mul_mul_correct = mul_mul_correct + a.answer
            elif a.question_id == "COD-integerencoding-sequence-11":
                short_ans_total = short_ans_total + 1
                short_ans_correct = short_ans_correct + a.answer
            else:
                break
        mco_rate = round((mul_one_correct/mul_one_total * 100),2)
        mcm_rate = round((mul_mul_correct/mul_mul_total * 100),2)
        sans_rate = round((short_ans_correct/short_ans_total * 100),2)

        if request.method == 'GET':
            result = str('isAgoodgirl( {"mco_rate":' + str(mco_rate)
                             + ',"mcm_rate":' + str(mcm_rate) +
                         ',"sans_rate":' + str(sans_rate) + '});')
            return HttpResponse(result)








    #     context = {
    #         'result_count': total,
    #     }
    #                 # latest_answer_list for
    #                 # total += answrer;
    # return render(request, 'gedbhandler/result_count.html', context)



        # return context
        # return HttpResponse(json.dumps(data), content_type='application/jsonp')
   # def count_total(self):
    #     total = 0;
    #     for a in latest_answer_list:
    #         total = total + a.answer
    #
    #     context = {
    #         'latest_answer_list': latest_answer_list,
    #         'total':total
    #
    #     }