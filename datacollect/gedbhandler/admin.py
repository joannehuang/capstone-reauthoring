# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Answer, Result, Student, Teacher, Course

admin.site.register(Answer)
admin.site.register(Result)
admin.site.register(Student)
admin.site.register(Teacher)
admin.site.register(Course)