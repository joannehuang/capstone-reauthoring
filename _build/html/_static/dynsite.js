/*  Author: Abelardo Pardo (<abelardo.pardo@sydney.edu.au>) */
/* Version: 170726 */
/* Variable to allow for UID set by other means (additional code needed) */
var given_uid = '';
var mco;
var mom;
var sans;
var here = document.getElementById("here");
var login = document.getElementById("login");
var clickhere = document.getElementById("clickhere");

function setValue(mo_c,mo_m,s_ans){
    mco = mo_c;
    mom = mo_m;
    sans = s_ans;
}


function dynsite_send_data(s, v, o) {
    var data = {}
    /* If loading locally, skip the logging */
    if (location.protocol == 'file:') {
	console.log('SEND:' + document.location.pathname + ',' + 
                    DOCUMENTATION_OPTIONS.CONTEXT + ',' + v + ',' + JSON.stringify(o));
	return;
    }

    if (DOCUMENTATION_OPTIONS.DATA_CAPTURE_URL != '') {
        data[DOCUMENTATION_OPTIONS.DATA_CAPTURE_CONTEXT_NAME] = DOCUMENTATION_OPTIONS.CONTEXT;
        data[DOCUMENTATION_OPTIONS.DATA_CAPTURE_SUBJECT_NAME] = s;
        data[DOCUMENTATION_OPTIONS.DATA_CAPTURE_VERB_NAME] = v;
        data[DOCUMENTATION_OPTIONS.DATA_CAPTURE_OBJECT_NAME] = JSON.stringify(o);
        $.ajax({
	    'type': DOCUMENTATION_OPTIONS.DATA_CAPTURE_METHOD,
	    'url': DOCUMENTATION_OPTIONS.DATA_CAPTURE_URL,
	    'data': data
        });
    }
}
/*****  duration form submission **** */
$(document).ready(function() {
    function init() {
	/* This is button. Parent is the form element */
	var form_el = $(this).parent();
        var ok_icon = form_el.parent().children("img");

	$(this).click(function(e){
	    e.preventDefault();
	    data = {};
	    data['activity-id'] = form_el.find('input[name = "duration-id"]').val();
	    data['value'] = form_el.find('select[name = "duration-value"]').val();
	    dynsite_send_data(given_uid, "activity-duration", data);
	    form_el.hide()
	    ok_icon.show()
	    return false;  //stop the actual form post !important!
	});
    }
    $(".reauthoring_duration_submit").each(init)
});
/*****  Generic form submission **** */
$(document).ready(function() {
    function init() {
	/* This is button. Parent is the form element */
	var form_el = $(this).closest('form');
    var ok_icon = form_el.parent().children("img");
    var text_area = form_el.parent().find("textarea:first-of-type");
    var form_id = form_el.attr("id");

    if (form_id == null) {
        form_id = form_el.find("input[name='form_id']").attr("value");
    }
    if (form_id == null) {
        form_id = '';
    }
    if (text_area != null && DOCUMENTATION_OPTIONS.DATA_CAPTURE_URL != '') {
        data_request = {
            'type': 'GET',
            'url': DOCUMENTATION_OPTIONS.DATA_CAPTURE_URL + '/../get_attribute',
            'data': {'context': DOCUMENTATION_OPTIONS.CONTEXT,
                     'key': form_id + '_' + text_area.attr("name")
            }
        }

        $.ajax(data_request).done(function(data) {     //display data after ajax get.
            if (typeof data === 'string' && data != "") {
                text_area.text(data);
            }
        });
    }
	$(this).click(function(e){
	    //e.preventDefault(); Comment it to see what happens.
	    data = {};
	    /* This is the default event name */
	    event_name = "form-submit";
        /* Store some initial fields*/
        data['url'] = document.URL;
        data['form_id'] = form_id;
	    /* Loop over the input elements in the form */
	    form_el.find('*').filter(':input').each(function(){
            if (this.name == "") {
                return;
            }
            /* And this to catch the event name from within the form */
            if (this.name == "event-name") {
                event_name = this.value;
                return;
            }
            /* Accumulate the rest of input fields */
            if (((this.type != "radio") && (this.type != "checkbox")) || this.checked) {
                    /* data['id'] = this.name;
                data['answer'] = this.value; */
                data[this.name] = this.value;
            }
	    });
	    /* Send! */
	    dynsite_send_data(given_uid, event_name, data);

        /* If button has class reauthoring_reload, fire the reload */
        if ($(this).hasClass('reauthoring_reload')) {
            document.location.reload(true);
            return false;
        }
	    form_el.hide()
	    ok_icon.show()
	    return true;  //stop the actual form post !important! Set to true to see what happen
	});
    }

    $(".reauthoring_submit").each(init)
});






/*****************************************************************************************/




$(document).ready(function () {
    /* Get the buttons to execute the correct functions when clicked */
    var this_el = $('.reauthoring_embedded_quiz_buttons');
    this_el.find('.reauthoring-grade').click(grade);
    this_el.find('.reauthoring-grade').css('display', 'inline');
    this_el.find('.reauthoring-grade').click(result_collect);
    this_el.find('.reauthoring-again').click(again);
    this_el.find('.reauthoring-solution').click(solution);
    this_el.find('.reauthoring-solution').click(result_typeofQuestion);
    //this_el.find('.reauthoring-solution').click(result_collect);
    this_el.find('.incorrect_icon').css('marginLeft','-23px');

    /* Background of the list and the correct/incorrect icons */
    this_el = $('.reauthoring_embedded_quiz_questions');
    this_el.find('li').css('background',
		      'none no-repeat 0 0').css('marginLeft', '2em');
    this_el.find('ol').css('listStyleType', 'upper-alpha');
    this_el.find('ul').css('listStyleType', 'upper-alpha');
});
/* XY Click widget */
$(document).ready(function() {
  $('div.xy_click img.xy_grid_img').each(function(e) {
    var offset = $(this).offset();
    var xoff = - $(this).width()/2;
    var yoff = $(this).height()/2;
    var ok_icon = $(this).parent().children('img.xy_ok_icon');
    var id_label = $(this).parent().parent().parent().parent().attr('id');
    data_request = {
            'type': 'GET',
            'url': DOCUMENTATION_OPTIONS.DATA_CAPTURE_URL + '/../get_attribute',
            'data': {'context': DOCUMENTATION_OPTIONS.CONTEXT,
                     'key': 'xy_click_' + id_label}
            };
    $.ajax(data_request).done(function(data) {
        if (data != "") {
            e_pageX = offset.left - xoff - ((data['x'] * xoff)/100);
            e_pageY = offset.top + yoff - ((data['y'] * yoff)/100);
            ok_icon.css('top', e_pageY - offset.top - 20);
            ok_icon.css('left', e_pageX - offset.left - 6);
            ok_icon.show();
        }
    });
  });
  $('div.xy_click img.xy_grid_img').click(function(e) {
    var offset = $(this).offset();
    var xoff = - $(this).width()/2;
    var yoff = $(this).height()/2;
    var ok_icon = $(this).parent().children('img.xy_ok_icon');
    data = {};
    data['id'] = $(this).parent().parent().parent().parent().attr('id');
    data['url'] = document.URL;
    data['title'] = $(document).find('title').text()
    data['x'] = Math.round(100 * (e.pageX - offset.left + xoff)/-xoff);
    data['y'] = Math.round(100 * (yoff - (e.pageY - offset.top))/yoff);
    dynsite_send_data(given_uid, "xy-click", data);
    ok_icon.css('top', e.pageY - offset.top - 20);
    ok_icon.css('left', e.pageX - offset.left - 6);
    ok_icon.show();
  });
});
/* Page now records an event upon loading */
$(document).ready(function() {
    data = {};
    data['url'] = document.URL;
    dynsite_send_data(given_uid, "resource-view", data);

});


/***** Expand collapse of sections *****/
$(document).ready(function (){
        //alert(mc_o);
    function init(){
	// get header & section, and add static classes
	var header = $(this);
	var section = header.parent();
	header.addClass("html-toggle-button");
	// helper to test if url hash is within this section
	function contains_hash(){
	    var hash = document.location.hash;
	    return hash && (section[0].id == hash.substr(1) ||
			    section.find(hash.replace(/\./g,"\\.")).length>0);
	}

	// helper to control toggle state
	function set_state(expanded, track) {
	    if (expanded) {
		section.addClass("expanded").removeClass("collapsed");
		section.children().show();
		event_name = "expand";
	    } else {
		section.addClass("collapsed").removeClass("expanded");
		section.children().hide();
		/* for :ref: span tag */
		section.children("span:first-child:empty").show();
		header.show();
		event_name = "collapse";
	    }

	    if (track && DOCUMENTATION_OPTIONS.DATA_CAPTURE_URL) {
		data = {};
		data["page"] = document.URL.replace(/#.*$/, "");
		data["section"] = section.attr("id");
		data["operation"] = event_name;
		dynsite_send_data(given_uid, 'activity-collapse-expand', data);
	    }
	}

	// initialize state
	set_state(section.hasClass("expanded") || contains_hash(),
		  contains_hash());

	// bind toggle callback
	header.click(function (){
	    set_state(!section.hasClass("expanded"), true);
	    $(window).trigger('cloud-section-toggled', section[0]);
	});

	// open section if user jumps to it from w/in page
	$(window).bind("hashchange", function () {
            if(contains_hash()) set_state(true, true);
	});
    }

    // Apply to activity with section attribute
    $(".activity.section > h2, .activity.section > h3, .activity.section > h4, .activity.section > h5, .activity.section > h6").each(init);

    // Apply to chapter sections!
    $("div.chapter-with-expand > div.section > h1, div.chapter-with-expand > div.section > h2, div.chapter-with-expand > div.section > h3, div.chapter-with-expand > div.section > h4, div.chapter-with-expand > div.section > h5, div.chapter-with-expand > div.section > h6").each(init);
});


/***** Embedded MCQ *****/
function grade_mcq(div_el) {
    /* Get all the answer elements */
    answer_input = div_el.find("form > ol.eqt-answer-list > li input:checked");
    // If none is clicked, return -1
    if (answer_input.length == 0) {
	return -1;
    }
    // Return the answer
    if (answer_input[0].value == 'C') {
	return 1;
    }
    return 0;
}

function grade_mc(div_el) {
    /* Get all the answer elements */
    answers = div_el.find("form > ol.eqt-answer-list > li > input");
    /* Loop over input element of each answer */
    for (i = 0, max = answers.length; i < max; i++) {
	var answer = answers[i];
	/* Check if there has been a mistake */
	if ((answer.checked && answer.value == "I") ||
	    ((!answer.checked) && answer.value == "C")) {
	    // Mistake! Answer is incorrect
	    return 0;
	}
    }
    // No disagreement. Answer is correct
    return 1;
}

function grade_fib(div_el) {
    /* Find answer */
    var qval = div_el.find("form > div.reauthoring_embedded_quiz-fib-answer input[name = 'question']")[0].value;
    /* Empty answer prompts no action */
    if (qval == "") {
	return -1;
    }
    /* Find the given value */
    var aval = div_el.find("form > div.reauthoring_embedded_quiz-fib-answer input[name = 'solution']")[0].getAttribute("value");

    // If they agree, correct.
    if (qval == aval) {
	return 1;
    }

    // Incorrect
    return 0;
}

function grade() {
    /* Get the div element to process from above */
    var div_el = $(this).parent().parent();

    div_id = div_el[0].getAttribute('class');
    if (div_id.substring(div_id.length - 4, div_id.length) == "-fib") {
	answer = grade_fib(div_el);
    } else if (div_id.substring(div_id.length - 3, div_id.length) == "-mc") {
	answer = grade_mc(div_el);
    } else {
	answer = grade_mcq(div_el);
    }

    /* Grade button pushed with no answer given. Finish */
    if (answer == -1) {
	return;
    }

    buttons_el = div_el.find(".reauthoring_embedded_quiz_buttons");
    // If answer is correct, insert text in button and visualize icon
    if (answer == '1') {
	buttons_el.find(".correct_icon").css('opacity', '1');
	buttons_el.find(".reauthoring-embedded-answer").text('Correct');
    } else {
	buttons_el.find(".incorrect_icon").css('opacity', '1');
	buttons_el.find(".reauthoring-embedded-answer").text('Incorrect');
    }
    //alert("Cool code!!");

    // Send the "embedded-question-grade" event
    data = {};
    data['question_id'] = div_el.attr('id');
    data['answer'] = String(answer);
    dynsite_send_data(given_uid, 'embedded-question', data);

    /* Change the visibility of the buttons */
    buttons_el.find(".reauthoring-grade").css('display', 'none');
    buttons_el.find(".reauthoring-again").css('display', 'inline');
    buttons_el.find(".reauthoring-solution").css('display', 'inline');
    //......
    // irrelevant lines are hidden.
    //......
    $.ajax({
            type: "POST",
            url: "http://127.0.0.1:8000/datacollect/answer/receive/",
            data: data,  //"question_id":a, "answer":b
            dataType: "JSONP",
            success: function(suc){
                 console.log('GOOD');
            },
            jsonp: 'joanne',//parameter for telling the server to retrieve callback function
            jsonpCallback: "isAgoodgirl",//name of call back function
           error: function(err){
                console.log('BAD');
            }

    })

};
    // // if (data) {
    //      $.ajax({
    //          type: "GET",
    //          url: "http://127.0.0.1:8000/datacollect/result/count/",
    //          data: {},  //"question_id":a, "answer":b
    //          dataType: "JSONP",
    //          success: function (ret) {
    //              pass;
    //          }
    //      });
    //    alert(JSON.stringify(data));
    //  }




function again() {
    // Hide answer
    up_div = $(this).parent();
    up_div.find("img").css('opacity', '0');
    up_div.find(".reauthoring-embedded-answer").text('');
    // Change the buttons
    up_div.find(".reauthoring-grade").css('display', 'inline');
    up_div.find(".reauthoring-again").css('display', 'none');
    up_div.find(".reauthoring-solution").css('display', 'none');

    // Clean all the form fields
    up_up_div = $(this).parent().parent();
    up_up_div.find("form input").attr('checked', false);
    up_up_div.find("form input[type='text']").val('');

    // And the solutions
    up_up_div.find(".result_icon").css('display', 'none');
    up_up_div.find(".result_icon").css('display', 'none');
}

function solution_mcq(div_el) {
    /* Ordered list elements with the answer */
    div_el.find("form > ol.eqt-answer-list li").each(function() {
	$(this).find("img.result_icon").css('display', '');
    });
    return;
}

function solution_fib(div_el) {
    /* Find answer value and set it to the value of the question field */
    var aval = div_el.find("form > div.reauthoring_embedded_quiz-fib-answer input[name = 'solution']")[0].getAttribute("value");
    div_el.find("form > div.reauthoring_embedded_quiz-fib-answer input[name = 'question']")[0].value = aval;
    div_el.find(".reauthoring_embedded_quiz_buttons img").css('opacity', '0');
    div_el.find(".reauthoring_embedded_quiz_buttons .reauthoring-embedded-answer").text('');
}

function solution() {
    /* Get the div element to process from above */
    var div_el = $(this).parent().parent();

    div_id = div_el[0].getAttribute('class');
    if (div_id.substring(div_id.length - 4, div_id.length) == "-fib") {
	solution_fib(div_el);
    } else {
	solution_mcq(div_el);
    }

    // Send the "embedded-question-again" event
    data = {};
    data['question_id'] = div_el.attr('id');
    data['answer'] = "-1";
    dynsite_send_data(given_uid, 'embedded-question', data);
}



/*********************************************************/
// function createdialog(width,height,bodycontent,title,removeable){
// 	if(document.getElementById("www_codefans_net")==null){
// 		/*创建窗口的组成元素*/
// 		var dialog = document.createElement("div");
// 		var dialogtitlebar= document.createElement("div");
// 		var dialogbody = document.createElement("div");
// 		var dialogtitleimg = document.createElement("span");
// 		var dialogtitle = document.createElement("span");
// 		var dialogclose = document.createElement("span");
// 		var closeaction = document.createElement("button");
// 		/*为窗口设置一个id，id如此怪异是为了尽量避免与其他用户取的id相同而出错*/
// 		dialog.id = "223238909";
// 		/*组装对话框标题栏,按从里到外的顺序组装*/
// 		dialogtitle.innerHTML = title;
// 		dialogtitlebar.appendChild(dialogtitleimg);
// 		dialogtitlebar.appendChild(dialogtitle);
// 		dialogtitlebar.appendChild(dialogclose);
// 		dialogclose.appendChild(closeaction);
// 		/*组装对话框主体内容*/
// 		if(bodycontent!=null){
// 			bodycontent.style.display = "block";
// 			dialogbody.appendChild(bodycontent);
// 		}
// 		/*组装成完整的对话框*/
// 		dialog.appendChild(dialogtitlebar);
// 		dialog.appendChild(dialogbody);
// 		/*设置窗口组成元素的样式*/
// 		var templeft,temptop,tempheight//the location of the window
// 		var dialogcssText,dialogbodycssText;//拼出dialog和dialogbody的样式字符串
// 		templeft = (document.body.clientWidth-width)/2;
// 		temptop = (document.body.clientHeight-height)/2;
// 		tempheight= height-30;
//     	dialogcssText= "position:absolute;background:#1a3365;padding:1px;border:4px;top:"+temptop+"px;left:"+templeft+"px;height:"+height+"px;width:"+width+"px;";
// 		dialogbodycssText = "width:100%;background:#000000;"+"height:" + tempheight + "px;";
// 		dialog.style.cssText = dialogcssText;
// 		dialogtitlebar.style.cssText = "height:30px;width:100%;background:url(/jscss/demoimg/201311/titlebar.png) repeat;cursor:move;";
// 		dialogbody.style.cssText 	= dialogbodycssText;
// 		dialogtitleimg.style.cssText = "float:left;height:20px;width:20px;background:url(/jscss/demoimg/201311/40.gif);"+"display:block;margin:4px;line-height:20px;";
// 		dialogtitle.style.cssText = "font-size:16px;float:left;display:block;margin:4px;line-height:20px;";
// 		dialogclose.style.cssText 	= "float:right;display:block;margin:4px;line-height:20px;";
// 		closeaction.style.cssText	= "height:20px;width:24px;border-width:1px;"+"background-image:url(/jscss/demoimg/201311/close.png);cursor:pointer;";
// 		/*为窗口元素注册事件*/
// 		var dialogleft = parseInt(dialog.style.left);
// 		var dialogtop = parseInt(dialog.style.top);
// 		var ismousedown = false;//whether mouse down
// 		/*close button event*/
// 		closeaction.onclick = function(){
// 			dialog.parentNode.removeChild(dialog);
// 		}
// 		/*window moving*/
// 		if(removeable == true){
// 			var ismousedown = false;
// 			var dialogleft,dialogtop;
// 			var downX,downY;
// 			dialogleft = parseInt(dialog.style.left);
// 			dialogtop = parseInt(dialog.style.top);
// 			dialogtitlebar.onmousedown = function(e){
// 			ismousedown = true;
// 			downX = e.clientX;
// 			downY = e.clientY;
// 			}
// 			document.onmousemove = function(e){
// 				if(ismousedown){
// 				dialog.style.top = e.clientY - downY + dialogtop + "px";
// 				dialog.style.left = e.clientX - downX + dialogleft + "px";
// 				}
// 			}
// 			/*松开鼠标时要重新计算当前窗口的位置*/
// 			document.onmouseup = function(){
// 				dialogleft = parseInt(dialog.style.left);
// 				dialogtop = parseInt(dialog.style.top);
// 				ismousedown = false;
// 			}
// 		}
// 		return dialog;
// 	}//end if
// }



function result_collect() {

    $.ajax({
        type: 'GET',
        url: "http://127.0.0.1:8000/datacollect/result/url/",
        dataType: "JSONP",
        data:{},
        success: function myFunction(ret){
            $("#result_count").html( 'Number of attempts:' + ret.total_attempt + '<br>'
                +  "Number of correct answer: " + ret.result_count);


        },
        error: function(){
             console.log('fail');
        },
        jsonp: 'joanne',
        jsonpCallback: "isAgoodgirl"
     })
}

function result_typeofQuestion() {

    $.ajax({
        type: 'GET',
        url: "http://127.0.0.1:8000/datacollect/result/count/",
        dataType: "JSONP",
        data:{},
        success: function myFunction(ret){
            setValue(ret.mco_rate, ret.mcm_rate, ret.sans_rate);
            barchart();

        },
        error: function(){
             console.log('fail');
        },
        jsonp: 'joanne',
        jsonpCallback: "isAgoodgirl"
     })
}
//$(document).ready(result_typeofQuestion);
$(document).ready(result_collect);
//Call back function for handling request in JSONP format.

function isAgoodgirl(json) {
    //do stuff...
}


function barchart(){
        var data = [
            {xAxis:'MC-O',value:mco},
            {xAxis:'MC-M',value:mom},
            {xAxis:'FIB',value:sans},

        ]
        var chart = new sBarChart('canvas',data,{
            title: 'Correct Rate in terms of Questions(%)',
            bgColor: '#829dba',
            titleColor: '#ffffff',      // 标题颜色
            titlePosition: 'top',       // 标题位置
            fillColor: '#72f6ff',       // 柱状填充色
            axisColor: '#eeeeee',       // 坐标轴颜色
            contentColor: '#bbbbbb'     // 内容横线颜色
        });
}
$(document).ready(barchart);
//flow window generation
// reference: https://blog.csdn.net/liushuijinger/article/details/20546859
function myshow(){
        //document.getElementById('mydiv').style.display = "none";
        document.getElementById('mydiv').style.width = "550px";
        document.getElementById('mydiv').style.height = "550px";
    } //block
    function myhide(){
        //document.getElementById('mydiv').style.display = "block";
        document.getElementById('mydiv').style.width="10px";
        document.getElementById('mydiv').style.height="10px";
    }

    new function(w,b,c,d,o){
         d=document;b=d.body;o=b.childNodes;c="className";
         b.appendChild(w=d.createElement("div"))[c]= "b";
         for(var i=0; i<o.length-1; i++)if(o[i][c]!="w")w.appendChild(o[i]),i--;
         (window.onresize = function(){
          w.style.width = d.documentElement.clientWidth + "px";
          w.style.height = d.documentElement.clientHeight + "px";
         })();}















