# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Teacher(models.Model):
    # id = models.AutoField(primary_key=True)
    teacher_number = models.IntegerField(default=0)
    teacher_name = models.CharField(max_length=200)

    def __str__(self):
        return self.teacher_name

class Course(models.Model):
    # id = models.AutoField(primary_key=True)
    course_number = models.IntegerField(default=0)
    course_name = models.CharField(max_length=200)
    teacher = models.ForeignKey(
        Teacher,
        on_delete=models.CASCADE)

    def __str__(self):
        return self.course_name

class Answer(models.Model):
    # id = models.AutoField(primary_key=True)
    question_id = models.CharField(max_length=200)
    create_time = models.DateTimeField(auto_now_add=True)
    answer =  models.IntegerField(default=0)
    course = models.ForeignKey(
        Course,
        on_delete=models.CASCADE)

    def __str__(self):
        return self.question_id

class Result(models.Model):
    # id = models.AutoField(primary_key=True)
    create_time = models.DateTimeField(auto_now_add=True)
    question_id = models.CharField(max_length=200)
    correct_rate = models.DecimalField(max_digits=3,decimal_places=2)
    answer = models.ForeignKey(
        Answer,
        on_delete=models.CASCADE)

    def __str__(self):
        return self.question_id


class Student(models.Model):
    # id = models.AutoField(primary_key=True)
    student_id = models.IntegerField(default=0)
    student_name = models.CharField(max_length=200)
    courses = models.ManyToManyField(Course)

    def __str__(self):
        return self.student_name








